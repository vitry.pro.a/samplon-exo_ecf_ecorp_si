CREATE TABLE `product` (
  `id_product` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `restaurant` (
  `id_restaurant` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `recipe` (
  `id_recipe` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `product_restaurant` (
  `id_restaurant` int,
  `id_product` int,
  `quantity` int NOT NULL,
  `date` date
);

CREATE TABLE `product_recipe` (
  `id_recipe` int,
  `id_product` int,
  `quantity` int NOT NULL
);

ALTER TABLE `product_recipe` ADD FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`);

ALTER TABLE `product_restaurant` ADD FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`);

ALTER TABLE `product_recipe` ADD FOREIGN KEY (`id_recipe`) REFERENCES `recipe` (`id_recipe`);

ALTER TABLE `product_restaurant` ADD FOREIGN KEY (`id_restaurant`) REFERENCES `restaurant` (`id_restaurant`);
