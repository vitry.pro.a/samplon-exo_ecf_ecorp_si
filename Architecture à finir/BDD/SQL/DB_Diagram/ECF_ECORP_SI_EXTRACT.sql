CREATE TABLE `extract_A` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `quantity` int NOT NULL
);

CREATE TABLE `extract_B` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `quantity` int NOT NULL,
  `date` date
);

CREATE TABLE `extract_C` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `quantity` int NOT NULL
);

CREATE TABLE `extract_recipe` (
  `id` int PRIMARY KEY,
  `name_recipe` varchar(255),
  `name_product` varchar(255),
  `quantity` int NOT NULL
);
