# 1ère partie : stocks.

On veut gérer les stocks de notre chaîne de restaurant pour éviter le gaspillage alimentaire (et faire des économies).
Pour l'instant, chaque restaurant à son propre système de gestion de stock.
Pour l’exemple, on vous fournit 3 extraits de BDD, chacune utilisant un système différent.

## Analyser les extraits, et faire un modèle de données pour chacun.

L’objectif est d’avoir une BDD interrogeable qui agrège tous les stocks de tous les restaurants.

## Créer un modèle de données pour la BDD commune.

On veut savoir, pour chaque produit, où il se trouve et en quelle quantité. Le “où”
Pour notre BDD on utilisera mysql.

## Implémenter le modèle de données pour MySQL dans un fichier init.sql

Il faut pouvoir importer les données des différents restaurants dans notre BDD.

## Créer 3 scripts en python, un pour chaque extrait, chacun ajoute les donnés de l’extrait à la BDD.

Par exemple un script load_A.py qui prend toutes les données de l’extrait A et les ajoute dans la BDD.
