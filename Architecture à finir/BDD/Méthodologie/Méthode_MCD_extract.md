# Analyser les extraits, et faire un modèle de données pour chacun.

Dans Analyse_data, les noms de colonne sont récupérés:

## extract_A.csv
- product: nom de produit
- stock: quantité
## extract_B.csv
- stock: nom de produit
- date: date au format JJ/MM/AAAA
- inventory: quantité
## extract_C.csv
- product: nom de produit
- stock: quantité
## recettes.csv
- ingredient: nom de produit
- spoon: ??? quantité
- name: nom de la recette

Puis pour chaque extract, on uniformise en fonction de la récurrence:

## extract_A.csv
- product: nom de produit
- quantity: quantité
## extract_B.csv
- product: nom de produit
- quantity: quantité
- date: date au format JJ/MM/AAAA
## extract_C.csv
- product: nom de produit
- quantity: quantité
## recettes.csv
- recipe: nom de la recette
- ingredient: nom de produit
- quantity: quantité