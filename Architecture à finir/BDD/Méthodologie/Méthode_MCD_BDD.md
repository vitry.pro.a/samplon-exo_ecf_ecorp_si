# Créer un modèle de données pour la BDD commune.

On veut savoir, pour chaque produit, où il se trouve et en quelle quantité. Le “où”
Pour notre BDD on utilisera mysql.

Data extract:
## extract_A.csv
- product: nom de produit
- quantity: quantité
## extract_B.csv
- product: nom de produit
- quantity: quantité
- date: date au format JJ/MM/AAAA
## extract_C.csv
- product: nom de produit
- quantity: quantité
## recettes.csv
- recipe: nom de la recette
- ingredient: nom de produit
- quantity: quantité

# Nous avons les entités suivantes:
- Restaurants: ici les extracts
- Produits: les produits des stocks et des recettes
- Recettes: les recettes

# Nous avons les associations suivantes:
- produit_restaurant: c'est cette table qui fait le liens entre la liste des produits qui existes et les restaurants, avec les quantités associés.
Elle permet de savoir combien de tel produits que j'ai dans tel restaurant.

- produit_recette: c'est cette table qui fait le liens entre la liste des produits qui existes et les recettes, avec les quantités associés.
Elle permet de savoir combien de tel produits que j'ai dans tel recette.

# Nous avons les attributs suivants:

![MCD_BDD](/BDD/Schema_MLD/MCD/MCD_BDD.png)