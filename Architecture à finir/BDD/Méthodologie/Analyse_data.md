# Liste des extracts:

## extract_A.csv
Data: 1ère ligne: nom de colonne
- product: nom de produit
        > Présence de doublon
- stock: quantité
        > Quantité à recompter (doublon)
## extract_B.csv
Data: 1ère ligne: nom de colonne
- stock: nom de produit
        > Présence de doublon
- date: date au format JJ/MM/AAAA
- inventory: quantité
        > Quantité à recompter (doublon)
## extract_C.csv
Data: pas de nom de colonne
- product: nom de produit
        > Présence de doublon
- stock: quantité (ici certaines sont négatives)
        > Quantité à recompter (doublon)
## recettes.csv
Data: 1ère ligne: nom de colonne
- ingredient: nom de produit
- spoon: ??? quantité
- name: nom de la recette