# 2ème partie : recettes.

On veut également utiliser cette base de données pour stocker la partie “ingrédient” des recettes que
l’on sert dans les restaurants.
Par exemple pour faire nos fameuses crêpes il faut :
- 1 oeuf
- 100g de farine
- 100g de sucre
- 100ml de lait
- 1g d'ingrédients secrets.

## Faire une deuxième version du modèle de données qui ajoute la ou les tables nécessaires pour stocker les recettes.

On ne veut pas modifier notre fichier init.sql !

## Créer un fichier migration.sql qui, s’il est exécuté après init.sql fait passer la BDD dans version 2 du modèle.

Ce qui veut dire dire que si je lance init.sql, ma BDD est dans la version 1. Si je lance ensuite
migration.sql, ma BDD est dans la version 2. Mes scripts d’import des données doivent fonctionner
quelque soit la version de la BDD.

## Pour tester, créez dans la BDD des recettes avec des produits disponibles et indisponibles en stock.

On veut également pouvoir intégrer nos super recettes secrètes !

## Faire un script qui permet d’intégrer des fichiers de recettes au même format que le fichier recette.csv
