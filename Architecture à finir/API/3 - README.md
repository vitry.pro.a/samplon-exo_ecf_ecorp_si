# 3ème partie : API.

 - On veut pouvoir accéder à nos données grâce à une API web. On veut :
 - En envoyant le nom d’un produit, connaître l’état des stocks dans chaque restaurant ainsi que le stock global (cumulé).
 - Pouvoir ajouter un nouveau produit.
 - Pouvoir augmenter ou diminuer le stock d’un produit dans un restaurant donné.
Attention le stock ne doit jamais être négatif !
 - Ajouter une recette (la liste de ses ingrédients et les quantités).
 - Pouvoir retrouver une recette grâce à son nom.
 - Pouvoir savoir combien de fois je peux faire une recette donnée dans chacun des restaurants. (celle là si vous n’y arrivez pas c’est pas grâve)