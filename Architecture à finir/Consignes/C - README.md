# ECF ECORP SI verison facile


> Modéliser et implémenté une BDD + la rendre accessible via une API.


## Contexte du projet
On vous founit un PDF qui exprime un besoin (avec quelques pages du département marketing pour vous aider).
Vous devez répondre à ce besoin.

## Modalités d'évaluation
Relecture du code + test avec un d'autres jeux de données et collections insomnia.

## Livrables
Repos GIT avec le code demander. (vérifier que la /docs de l'API soit compréhensible et fonctionne correctement)