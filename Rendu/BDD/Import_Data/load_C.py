import pandas as pd
import mysql.connector
from tabulate import tabulate

from BDD import save_in_bdd, sqlalchemy_engine, cursor_mysql
from utiles import column_lower, removes_s_accent, insert_table

mydb = mysql.connector.connect(
    host="localhost",
    user="antony",
    password="choupette",
    database="ECF_ECORP_SI"
)

if "__main__" == __name__:
    # Test d'import via sqlalchemy pour lecture d'extract
    extract_bool = False

    # Step 0: Initialisation cursor mysql.connector
    mycursor = mydb.cursor()

    # Step 1: Create a SQLAlchemy engine to connect to the MySQL database
    engine_extract = sqlalchemy_engine("EXTRACT_ECF_ECORP_SI")

    # Step 2: Create a DataFrame with the data
    df = pd.read_csv('EXTRACT/extract_C.csv',names=['name', 'quantity'], index_col=False)
    
    # Step 3: Formating data
        # Minuscules
    column_lower(df, "name")
        # Accents and Word withless s
    removes_s_accent(df, "name")
        # Quantité négative
    df['quantity'][df['quantity'] < 0] = 0
        # Doublon
    df_grouped = df.groupby(['name']) 
    df_bdd = df_grouped.sum().reset_index()
    
    # Step 4: Verification - Displaying the DataFrame
    print(tabulate(df_bdd, headers="keys", tablefmt="pretty"))

    # Step 5: Convert the Pandas DataFrame to a format for MySQL table insertion
    if extract_bool == True:
        save_in_bdd(df_bdd, 'extract_C', engine_extract)

    # Step 6: Ajoute le dataframe dans la BDD
    nom_restaurant_C = "Restaurant_C" # nom du restaurant pour le fichier load_C

    insert_table(mydb, mycursor, nom_restaurant_C, df_bdd, "name", "product", True)
