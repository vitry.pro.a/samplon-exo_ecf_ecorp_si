import pandas as pd
import mysql.connector
from tabulate import tabulate

from BDD import save_in_bdd, sqlalchemy_engine
from utiles import column_lower, removes_s_accent, insert_table_recipe

mydb = mysql.connector.connect(
    host="localhost",
    user="antony",
    password="choupette",
    database="ECF_ECORP_SI"
)

if "__main__" == __name__:
    # Test d'import via sqlalchemy pour lecture d'extract
    extract_bool = False

    # Step 0: Initialisation cursor mysql.connector
    mycursor = mydb.cursor()

    # Step 1: Create a SQLAlchemy engine to connect to the MySQL database
    engine_extract = sqlalchemy_engine("EXTRACT_ECF_ECORP_SI")

    # Step 2: Create a DataFrame with the data
    df = pd.read_csv('EXTRACT/recettes.csv')

    # Step 3: Formating data
        # New name of column: ingredient,spoon,name
    df = df.rename(columns={'ingredient': 'name_product','spoon': 'quantity', 'name': 'name_recipe'})
        # Minuscules
    column_lower(df, "name_product")
    column_lower(df, "name_recipe")
        # Accents and Word withless s
    removes_s_accent(df, "name_product")
    removes_s_accent(df, "name_recipe")
        # Doublon
    # df_grouped = df.groupby(['name_product']) 
    # df_bdd = df_grouped.sum().reset_index()
    
    # Step 4: Verification - Displaying the DataFrame
    print(tabulate(df, headers="keys", tablefmt="pretty"))

    # Step 5: Convert the Pandas DataFrame to a format for MySQL table insertion
        # Uniquement dans la base de donnée Extract
    if extract_bool == True:
        save_in_bdd(df, 'extract_recipe', engine_extract)

    # Step 6: Ajoute le dataframe dans la BDD
        """ Cette fonction ne marche pas car le copier coller était plus compliqué que prévu
         Par manque de temps 0H00, l'insertion de les recettes ne fonctionne pas """
    insert_table_recipe(mydb, mycursor, df, "name", "product")
