from unidecode import unidecode
from datetime import datetime, date

debug_print = True

def print_under_condition(data, bool):
    """print_under_condition
    Args:
        data (_type_): donnée à print
        bool (_type_): True: s'affiche / False: rien
    """
    if bool == True:
        print(data)


def column_lower(dataframe, column):
    """column_lower
    Args:
        dataframe (_type_): _description_
        column (_type_): _description_
    """
    # Applique le lowercase sur la colonne du DF
    dataframe[column] = dataframe[column].str.lower()


def column_upper(dataframe, column):
    """column_upper: Non utilisé
    Args:
        dataframe (_type_): _description_
        column (_type_): _description_
    """
    # Applique le uppercase sur la colonne du DF
    dataframe[column] = dataframe[column].str.upper()

def test_date(dataframe, column):
    """Applique le formatage et le trie de date sur la colonne du DF
    Args:
        dataframe (_type_): _description_
        column (_type_): _description_
    """
    for ind, line in enumerate(dataframe[column]):
        liste_date = []
        for i in line.split("/2023"):
            liste_tmp = i.split("/")
            if liste_tmp[0] != '' and len(liste_tmp) < 3:
                liste_tmp.append('2023')
                liste_date.append("-".join(liste_tmp))
                sorted_lst = sorted(liste_date, 
                                    key=lambda x: datetime.strptime(x, '%Y-%m-%d'), 
                                    reverse=True)
        dataframe.loc[dataframe.index[ind], column] = sorted_lst[0]


def date_format(dataframe, column):
    """
    Index: 0  - Ligne df in for: 24/6/2023  - Ligne df in name: parfum
    Test if:
    Test condition si:
    Test if_1 format liste_date: 2023-06-24
    Test suite for:
    Test valeur dataframe: 24/6/2023
    Test format liste_date: 2023-06-24
    Args:
        dataframe (_type_): _description_
        column (_type_): _description_
    """
    # Applique le formatage et le trie de date sur la colonne du DF
    print_under_condition(("Test len: ", len(dataframe[column])), debug_print)
    print_under_condition(('Test start For:'), debug_print)
    for ind, line in enumerate(dataframe[column]):
        print_under_condition(("Index:", ind,
                               " - Ligne df in for:", line,
                               " - Ligne df in name:", dataframe.loc[ind, "name"]), 
                               debug_print)
        liste_date = []
        for i in line.split("/2023"):
            liste_tmp = i.split("/")
            print_under_condition(("Test if:"), debug_print)
            if liste_tmp[0] != '' and len(liste_tmp) < 3:
                print_under_condition(("Test condition si:"), debug_print)
                liste_tmp.append('2023')
                str_date = "-".join(liste_tmp)
                liste_date.append(datetime.strptime(str_date, '%d-%m-%Y').date())
                print_under_condition(("Test if_1 format liste_date:", liste_date[0]), 
                                      debug_print)
                break
            elif len(liste_tmp) == 3: 
                print_under_condition(("Test condition sinon:"), debug_print)
                str_date = "-".join(liste_tmp)
                liste_date.append(datetime.strptime(str_date, '%d-%m-%Y').date())
                print_under_condition(("Test if_2 format liste_date:", liste_date[0]), debug_print)
                break
        print_under_condition(("Test suite for:"), debug_print)
        print_under_condition(("Test valeur dataframe:", dataframe.loc[ind, column]), 
                              debug_print)
        print_under_condition(("Test format liste_date:" ,datetime.strptime(str(liste_date[0]), '%Y-%m-%d').date()), 
                              debug_print)
        dataframe.loc[dataframe.index[ind], column] = liste_date[0]


def removes_s_accent(dataframe, column):
    """ Modifie le dataframe envoyer
    Args:
        dataframe (dataframe): Dataframe à modifier
        column (str): colonne du dataframe à modifier
    """
    for ind, line in enumerate(dataframe[column]):
        list_line = line.split()
        nouvelle_expr = []
        for word in list_line:
            # enlever les s à la fin des mots
            tmp_s_less = word[:-1] if word.endswith('s') else word
            # enlever les accents et les trucs louches (genre les accents et les œ)
            tmp_accent = unidecode(tmp_s_less)
            # nouvelle_expr ajouter
            nouvelle_expr.append(tmp_accent)
        # nouvelle ligne construite avec join
        new_line = " ".join(nouvelle_expr)
        # Modifie la valeur du dataframe
        dataframe.loc[dataframe.index[ind], column] = new_line


def add_1_value(connector, cursor, table, column, condition):
    """add_1_value
    Args:
        connector (_type_): _description_
        cursor (_type_): _description_
        table (_type_): _description_
        column (_type_): _description_
        condition (_type_): _description_
    """
    sql = f"INSERT INTO `{table}` ({column}) VALUES (%s);"
    val = (condition)
    cursor.execute(sql, val)
    connector.commit()


def request_bdd(cursor, table, column_table, column_condition, condition, unique=False):
    """request_bdd
    Args:
        cursor (_type_): _description_
        table (_type_): _description_
        column_table (_type_): _description_
        column_condition (_type_): _description_
        condition (_type_): _description_
        unique (bool, optional): _description_. Defaults to False.
    Returns:
        _type_: _description_
    """
    if unique == True:
        commande = "DISTINCT"
    else:
        commande = ""
    sql = f"SELECT {commande} `{column_table}` FROM `{table}` WHERE `{column_condition}` = %s;"
    val = ([condition])
    cursor.execute(sql, val)
    return cursor


def last_id(cursor):
    """request last_id
    Args:
        cursor (_type_): _description_
    Returns:
        _type_: _description_
    """
    sql = "SELECT LAST_INSERT_ID();"
    cursor.execute(sql)
    return cursor


def insert_relation_table(connector, cursor, dataframe, table, index, values, date):
    # table = "product_restaurant"
    sql = """
        SELECT DISTINCT COLUMN_NAME 
        FROM INFORMATION_SCHEMA.COLUMNS 
        WHERE TABLE_NAME = %s;"""
    val = ([table])
    cursor.execute(sql, val)
    # Liste des colonnes de la table
    column_liste = cursor.fetchall()
    if date == True:
        values.insert(0, datetime.today().strftime('%Y-%m-%d')) # date test: '2023-12-23'
    else:
        values.insert(0, dataframe.iloc[index, 1])
    # Liste des valeurs à inserer dans la BDD
    col_str = ", ".join([col[0] for col in column_liste])
    print_under_condition(("table:", table, "/ col:", col_str, "/ val:", values), 
                          debug_print)
    sql = f"INSERT INTO `{table}` ({str(col_str)}) VALUES (%s, %s, %s, %s);"
    val = (values)
    cursor.execute(sql, val)
    connector.commit()

#                   mydb, mycursor, nom_restaurant_A, df_bdd, "name", "product", True
def insert_table(connector, cursor, name_restaurant, dataframe, df_column, current_table, bool_date):
    """Insert dans une table de la bdd
    Args:
        name_restaurant (_type_): Restaurant du df / Fichier load
        dataframe (_type_): dataframe - data d'émission
        df_column (_type_): colonne du dataframe à inserer
        current_table (_type_): table de la bdd - data de réception
        current_column (_type_): ???
    """
    for index, donnee in enumerate(dataframe[df_column]):# df_bdd["name"]
        print_under_condition(("Test en cours:",index, donnee), debug_print)
        # Etape 0: Set variable selon extract (restaurant A/C ou B)
        if name_restaurant == "Restaurant_B":
            name_of_df = dataframe.iloc[index, 0]
            quantity_of_df = dataframe.iloc[index, 2]
        else:
            name_of_df = dataframe.loc[index, "name"]
            quantity_of_df = dataframe.loc[index, "quantity"]
        print_under_condition(("df_bdd:", name_of_df, name_of_df), debug_print)
        # Etape 1: Ajoute et vérifie le restaurant dans la BDD
        try:
            # Requête SQL: Récupération restaurant selon une condition
            print_under_condition(("Test 0 - TRY_start_restaurant"), debug_print)
            restaurant_name = request_bdd(cursor, "restaurant", "name", "name", name_restaurant)
            restaurant_test = restaurant_name.fetchone()[0]
            print_under_condition(("Test 1 - CHECK - Retour bdd: restaurant_name est", restaurant_test), 
                                  debug_print)
            # Condition de retour: bdd != null
            if restaurant_test == name_restaurant:
                # Requête SQL: Récupération restaurant_id de la BDD
                restaurant_id_req = request_bdd(cursor, "restaurant", "id_restaurant", "name", name_restaurant)
                restaurant_id = restaurant_id_req.fetchone()[0]
                print_under_condition(("Test 2 - CHECK - Retour bdd: restaurant_id est", restaurant_id), 
                                      debug_print)
        except:
            # Insertion data dans une colonne (ici nom)
            add_1_value(connector, cursor, "restaurant", "name", [name_restaurant])
            # Récupération du dernier id
            result_req = last_id(cursor)
            restaurant_id = result_req.fetchone()[0]
            print_under_condition(("Test 3 - ADD - last id restaurant:", restaurant_id), 
                                  debug_print)
        
        # Etape 2: Ajoute et vérifie le produit, la quantité et la date dans la BDD
        try:
            # Requête SQL: Récupération nom produit
            print_under_condition(("Test 0 - TRY_start_product"), debug_print)
            resultat = request_bdd(cursor, current_table, df_column, df_column, name_of_df)
            line_test = resultat.fetchone()[0]
            print_under_condition(("Test 1 - CHECK - Retour bdd: product_id est", line_test), 
                                  debug_print)
            # Condition de retour: bdd != null
            if line_test == name_of_df:
                # Requête SQL: Récupération nom produit
                product_id_req = request_bdd(cursor, current_table, "id_product", df_column, name_of_df)
                product_id = product_id_req.fetchone()[0]
                print_under_condition(("Test 2 - CHECK - Retour bdd: product_id est", product_id), 
                                      debug_print)
                # Requête SQL: Insertion (Table de relation)
                    # Format data: [id_restaurant, id_product, quantity, date]
                print_under_condition(("Test 4 - ADD - Valeur:", product_id, restaurant_id, quantity_of_df), 
                                      debug_print)
                valeur = [str(product_id), str(restaurant_id), str(quantity_of_df)]
                    # Format argument: table, values, date
                insert_relation_table(connector, cursor, dataframe, "product_restaurant", index, valeur, bool_date)
        except:
            # Requête SQL: Insertion nom produit
            add_1_value(connector, cursor, current_table, df_column, [name_of_df])
            # Requête SQL: Récupération du dernier id
            result = last_id(cursor)
            product_id_except = result.fetchone()[0]
            print_under_condition(("Test 3 - ADD - last id produit:", product_id_except), 
                                  debug_print)
            # Requête SQL: Insertion (Table de relation)
                    # Format data: [id_restaurant, id_product, quantity, date]
            print_under_condition(("Test 4 - ADD - Valeur:", product_id_except, restaurant_id, quantity_of_df), 
                                  debug_print)
            valeur = [str(product_id_except), str(restaurant_id), str(quantity_of_df)]
                    # Format argument: table, values, date
            insert_relation_table(connector, cursor, dataframe, "product_restaurant", index, valeur, bool_date)

def insert_table_recipe(connector, cursor, dataframe, df_column, current_table):
    # Méthodes:
        # Mettre le produit dans bdd OK FAIT
            # si produit existe TRY OK FAIT
        # Mettre le recipe dans bdd OK FAIT
            # si recipe existe TRY OK FAIT
        # Mettre l'id produit / l'id recipe / la quantity dans bdd
            # SELECT id produit
            # SELECT id recipe
            # DF quantity
    for index, donnee in enumerate(dataframe[df_column]):# df["name"]
        print_under_condition(("Test en cours:",index, donnee), debug_print)
        name_recipe_of_df = dataframe.loc[index, "name_recipe"]
        name_of_df = dataframe.loc[index, "name_product"]
        quantity_of_df = dataframe.loc[index, "quantity"]

        # Etape 1: Ajoute et vérifie le recipe dans la BDD
        try:
            # Requête SQL: Récupération recipe selon une condition
            print_under_condition(("Test 0 - TRY_start_recipe"), debug_print)
            #                       cursor, table, column_table, column_condition, condition, unique=False
            recipe_name = request_bdd(cursor, "recipe", "name", "name", name_recipe_of_df)
            recipe_test = recipe_name.fetchone()[0]
            print_under_condition(("Test 1 - CHECK - Retour bdd: name_recipe est", recipe_test), 
                                    debug_print)
            # Condition de retour: bdd != null
            if recipe_test == name_recipe_of_df:
                # Requête SQL: Récupération recipe_id de la BDD
                recipe_id_req = request_bdd(cursor, "recipe", "id_recipe", "name", name_recipe_of_df)
                recipe_id = recipe_id_req.fetchone()[0]
                print_under_condition(("Test 2 - CHECK - Retour bdd: recipe_id est", recipe_id), 
                                        debug_print)
        except:
            # Insertion data dans une colonne (ici nom)
            add_1_value(connector, cursor, "recipe", "name", [name_recipe_of_df])
            # Récupération du dernier id
            result_req = last_id(cursor)
            recipe_id = result_req.fetchone()[0]
            print_under_condition(("Test 3 - ADD - last id recipe:", recipe_id), 
                                    debug_print)

        # Etape 2: Ajoute et vérifie le produit, la quantité et la date dans la BDD
        try:
            # Requête SQL: Récupération nom produit
            print_under_condition(("Test 0 - TRY_start_product"), debug_print)
            # cursor, table, column_table, column_condition, condition, unique=False
            resultat = request_bdd(cursor, current_table, df_column, df_column, name_of_df)
            line_test = resultat.fetchone()[0]
            print_under_condition(("Test 1 - CHECK - Retour bdd: product_id est", line_test), 
                                    debug_print)
            # Condition de retour: bdd != null
            if line_test == name_of_df:
                # Requête SQL: Récupération nom produit
                product_id_req = request_bdd(cursor, current_table, "id_product", df_column, name_of_df)
                product_id = product_id_req.fetchone()[0]
                print_under_condition(("Test 2 - CHECK - Retour bdd: product_id est", product_id), 
                                        debug_print)
                # Requête SQL: Insertion (Table de relation)
                    # Format data: [id_recipe, id_product, quantity, date]
                print_under_condition(("Test 4 - ADD - Valeur:", product_id, recipe_id, quantity_of_df), 
                                        debug_print)
                valeur = [str(product_id), str(recipe_id), str(quantity_of_df)]
                    # Format argument: table, values, date
                # insert_relation_table(connector, cursor, dataframe, "product_recipe", index, valeur, bool_date)
        except:
            # Requête SQL: Insertion nom produit
            add_1_value(connector, cursor, current_table, df_column, [name_of_df])
            # Requête SQL: Récupération du dernier id
            result = last_id(cursor)
            product_id_except = result.fetchone()[0]
            print_under_condition(("Test 3 - ADD - last id produit:", product_id_except), 
                                    debug_print)
            # Requête SQL: Insertion (Table de relation)
                    # Format data: [id_recipe, id_product, quantity, date]
            print_under_condition(("Test 4 - ADD - Valeur:", product_id_except, recipe_id, quantity_of_df), 
                                    debug_print)
            valeur = [str(product_id_except), str(recipe_id), str(quantity_of_df)]
                    # Format argument: table, values, date
            # insert_relation_table(connector, cursor, dataframe, "product_recipe", index, valeur, bool_date)