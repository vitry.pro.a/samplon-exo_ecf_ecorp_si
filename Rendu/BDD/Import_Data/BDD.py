import pandas as pd
from sqlalchemy import create_engine
import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="antony",
    password="choupette",
    database="ECF_ECORP_SI"
)

# Create a SQLAlchemy engine to connect to the MySQL database
def sqlalchemy_engine(bdd):
    return create_engine(f"mysql+mysqlconnector://antony:choupette@localhost/{bdd}")

# Convert the Pandas DataFrame to a format for MySQL table insertion
def save_in_bdd(dataframe, table, connexion):
    dataframe.to_sql(table, con=connexion, if_exists='replace', index = False)

def cursor_mysql(): # non UTILISÉ
    return mydb.cursor()