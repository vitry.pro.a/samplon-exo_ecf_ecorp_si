import pandas as pd
import mysql.connector
from tabulate import tabulate
from datetime import datetime, date

import BDD
from utiles import column_lower, removes_s_accent, test_date, date_format, insert_table
from operator import itemgetter, attrgetter

mydb = mysql.connector.connect(
    host="localhost",
    user="antony",
    password="choupette",
    database="ECF_ECORP_SI"
)

if "__main__" == __name__:
    # Test d'import via sqlalchemy pour lecture d'extract
    extract_bool = False

    # Deux type de trie le bon est sur False car l'autre n'envoie pas les bonnes données
    sort_date = False

    # Step 0: Initialisation cursor mysql.connector
    mycursor = mydb.cursor()

    # Step 1: Create a SQLAlchemy engine to connect to the MySQL database
    engine_extract = BDD.sqlalchemy_engine("EXTRACT_ECF_ECORP_SI")

    # Step 2: Create a DataFrame with the data
    df = pd.read_csv('EXTRACT/extract_B.csv')

    # Step 3: Formating data
        # New name of column / stock, date, inventory
    df = df.rename(columns={'stock': 'name','inventory': 'quantity'})
        # Minuscules
    column_lower(df, "name")
        # Accents and Word withless s
    removes_s_accent(df, "name")
    date_format(df, "date")
        # Doublon
    if sort_date == True:
        df_grouped = df.groupby(['name'])
        df_bdd = df_grouped.sum().reset_index()
            # Lorsque groupby par "name" / formating date -> Fait la somme des quantités
        test_date(df_bdd, "date")
    else:
            # Lorsque groupby par "name" / formating date -> 1 date la plus récente
        df_bdd = df.groupby(["name"]).apply(
            lambda x: x.sort_values(["date"], ascending = True).reset_index(drop=True)).drop_duplicates(["name"],keep='last')
     
    # Step 4: Verification - Displaying the DataFrame
    print(tabulate(df_bdd, headers="keys", tablefmt="pretty"))

    # Step 5: Convert the Pandas DataFrame to a format for MySQL table insertion
        # Uniquement dans la base de donnée Extract
    if extract_bool == True:
        BDD.save_in_bdd(df_bdd, 'extract_B', engine_extract)

    # Step 6: Ajoute le dataframe dans la BDD
    nom_restaurant_B = "Restaurant_B" # nom du restaurant pour le fichier load_B

    # i = 1
    # print(df_bdd.iloc[i, 0], df_bdd.iloc[i, 1], df_bdd.iloc[i, 2])
    insert_table(mydb, mycursor, nom_restaurant_B, df_bdd, "name", "product", False)
