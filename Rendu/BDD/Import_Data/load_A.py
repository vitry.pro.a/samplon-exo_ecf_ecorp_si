import pandas as pd
import mysql.connector
from tabulate import tabulate

from BDD import save_in_bdd, sqlalchemy_engine
from utiles import column_lower, removes_s_accent, insert_table

mydb = mysql.connector.connect(
    host="localhost",
    user="antony",
    password="choupette",
    database="ECF_ECORP_SI"
)

if "__main__" == __name__:
    # Test d'import via sqlalchemy pour lecture d'extract
    extract_bool = False

    # Step 0: Initialisation cursor mysql.connector
    mycursor = mydb.cursor()

    # Step 1: Create a SQLAlchemy engine to connect to the MySQL database
    engine_extract = sqlalchemy_engine("EXTRACT_ECF_ECORP_SI")

    # Step 2: Create a DataFrame with the data
    df = pd.read_csv('EXTRACT/extract_A.csv')

    # Step 3: Formating data
        # New name of column
    df = df.rename(columns={'product': 'name','stock': 'quantity'})
        # Minuscules
    column_lower(df, "name")
        # Accents and Word withless s
    removes_s_accent(df, "name")
        # Doublon
    df_grouped = df.groupby(['name']) 
    df_bdd = df_grouped.sum().reset_index()
    
    # Step 4: Verification - Displaying the DataFrame
    print(tabulate(df_bdd, headers="keys", tablefmt="pretty"))

    # Step 5: Convert the Pandas DataFrame to a format for MySQL table insertion
        # Uniquement dans la base de donnée Extract
    if extract_bool == True:
        save_in_bdd(df_bdd, 'extract_A', engine_extract)

    # Step 6: Ajoute le dataframe dans la BDD
    nom_restaurant_A = "Restaurant_A" # nom du restaurant pour le fichier load_A

    insert_table(mydb, mycursor, nom_restaurant_A, df_bdd, "name", "product", True)