DROP DATABASE IF EXISTS EXTRACT_ECF_ECORP_SI;
CREATE DATABASE EXTRACT_ECF_ECORP_SI
  DEFAULT CHARACTER SET = 'utf8mb4';
USE EXTRACT_ECF_ECORP_SI;

CREATE USER 'antony'@'localhost' IDENTIFIED BY 'choupette';
GRANT ALL PRIVILEGES ON EXTRACT_ECF_ECORP_SI TO 'antony'@'localhost';

DROP TABLE IF EXISTS `extract_A`;
CREATE TABLE `extract_A` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `quantity` int NOT NULL
);

DROP TABLE IF EXISTS `extract_B`;
CREATE TABLE `extract_B` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `quantity` int NOT NULL,
  `date` date
);

DROP TABLE IF EXISTS `extract_C`;
CREATE TABLE `extract_C` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `quantity` int NOT NULL
);

DROP TABLE IF EXISTS `extract_recipe`;
CREATE TABLE `extract_recipe` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name_recipe` varchar(255),
  `name_product` varchar(255),
  `quantity` int NOT NULL
);