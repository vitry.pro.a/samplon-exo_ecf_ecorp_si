USE ECF_ECORP_SI;

DROP TABLE IF EXISTS `recipe`;
CREATE TABLE `recipe` (
  `id_recipe` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) UNIQUE
);

DROP TABLE IF EXISTS `product_recipe`;
CREATE TABLE `product_recipe` (
  `id_recipe` int,
  `id_product` int,
  `quantity` int NOT NULL
);

ALTER TABLE `product_recipe` ADD FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`);
ALTER TABLE `product_recipe` ADD FOREIGN KEY (`id_recipe`) REFERENCES `recipe` (`id_recipe`);