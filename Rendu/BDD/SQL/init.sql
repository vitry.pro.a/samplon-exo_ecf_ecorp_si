DROP DATABASE IF EXISTS ECF_ECORP_SI;
CREATE DATABASE ECF_ECORP_SI
  DEFAULT CHARACTER SET = 'utf8mb4';
USE ECF_ECORP_SI;

CREATE USER 'antony'@'localhost' IDENTIFIED BY 'choupette';
GRANT ALL PRIVILEGES ON ECF_ECORP_SI TO 'antony'@'localhost';

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id_product` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) UNIQUE
);

DROP TABLE IF EXISTS `restaurant`;
CREATE TABLE `restaurant` (
  `id_restaurant` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) UNIQUE
);

DROP TABLE IF EXISTS `product_restaurant`;
CREATE TABLE `product_restaurant` (
  `id_restaurant` int,
  `id_product` int,
  `quantity` int NOT NULL,
  `date` date
);

ALTER TABLE `product_restaurant` ADD FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`);
ALTER TABLE `product_restaurant` ADD FOREIGN KEY (`id_restaurant`) REFERENCES `restaurant` (`id_restaurant`);